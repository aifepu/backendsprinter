<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopDelivery extends Model
{
    use SoftDeletes;
    protected $table = 'shop_delivery';
    protected $hidden = ['deleted_at'];
    /**
     * Valores asignables en la base de datos
     *
     * @var array
     */
    protected $fillable = ['id_shop', 'price_total', 'email'];

    /**
     * Obtener los productos de un pedido
     */
    public function products() {
        return $this->hasMany('App\ShopDeliveryProduct', 'delivery_id');
    }

    /**
     * Sobreescritura del método boot para borrar las líneas de pedido relacionadas
     */
    protected static function boot() {
        parent::boot();
    
        static::deleted(function ($delivery) {
          $delivery->products()->delete();
        });
    }
}
