<?php

namespace App\Http\Controllers;

use App\ShopDelivery;
use App\ShopDeliveryProduct;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Cache;

use DB;
use Log;

class ShopDeliveryController extends Controller
{
    /**
     * Listado de los pedidos almacenados
     * Petición con 1 minuto de caché
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //añadimos la petición en cache de 1 minuto
        $deliveries = Cache::remember('delivery', 60, function() { 
            Log::info("[CACHE] Escribimos en cache la respuesta de api/delivery");
            return ShopDelivery::all(); 
        });

        Log::info("[GET] Se obtienen todas los pedidos disponibles -- Status petición: 200");
        return response()->json($deliveries, 200);
    }

    /**
     * Información sobre el pedido dada por id
     * 1 minuto de caché para la petición si existe registro
     * 
     * @param integer $shopDelivery parametro con el id del pedido
     * @return \Illuminate\Http\Response success->200 | not found->404
     */
    public function show($shopDelivery) {
        try {
            //comprobación de si existe el registro almacenado en cache
            if(Cache::has("api/delivery/$shopDelivery")) { $delivery = Cache::get("api/delivery/$shopDelivery"); }
            else {
                $delivery = ShopDelivery::findOrFail($shopDelivery);
                $products = $delivery->products;
                Log::info("[CACHE] Escribimos en cache la respuesta de api/delivery/$shopDelivery");
                Cache::put("api/delivery/$shopDelivery", $delivery, 60);
            }

            Log::info("[SHOW] Se obtiene el pedido con id: $shopDelivery -- Status petición: 200");
            return response()->json($delivery, 200);

        } catch(ModelNotFoundException $e) {
            Log::error("[GET] No se encuentra el pedido con id: $shopDelivery -- Status petición: 404");
            return response()->json('Not found', 404);
        }
    }

    /**
     * Almacenamiento de un nuevo pedido con sus líneas de producto
     * Petición sin caché
     *
     * @param  \Illuminate\Http\Request  $request petición con cabecera y líneas de producto añadidas
     * @return \Illuminate\Http\Response success->201 | validacion erronea del request->422 | error->500
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'id_shop' => 'required|integer',
                'price_total' => 'required',
                'email' => 'required',
                'products' => 'required|array',
            ]);

            //pedido
            $delivery = ShopDelivery::create($request->all());

            //productos del pedido
            foreach($request->input('products') as $product){
                $lineProduct = new ShopDeliveryProduct();
                $lineProduct->delivery_id = $delivery->id;
                $lineProduct->id_article = $product['id_article'];
                $lineProduct->units = $product['units'];
                $lineProduct->price_unit = $product['price_unit'];
                $lineProduct->price_total = $product['price_total'];
                $lineProduct->save();
            }
            DB::commit();
            
            Log::info("[POST] Nuevo pedido añadido con id: $delivery->id -- Status petición: 201");
            return response()->json('OK', 201);

        } catch( \ValidationException $a) {
            Log::error("[POST] No se pudo añadir el pedido validate -- Status petición: 422");
            return response()->json('Not found', 422);

        }catch( \Exception $e) {
            DB::rollBack();
            
            Log::error("[POST] No se pudo añadir el pedido  -- Status petición: 500");
            return response()->json('Not found', 500);
        }
    }

    /**
     * Eliminación de un pedido dado su id con sus líneas de pedido
     * Petición sin caché
     *
     * @return \Illuminate\Http\Response success->200 | not found->404 | error->500 | bad request->400
     */
    public function destroy($shopDelivery)
    {
        DB::beginTransaction();
        try {
            if(is_numeric($shopDelivery)) {
                ShopDelivery::findOrfail($shopDelivery);
                ShopDelivery::destroy($shopDelivery);
                DB::commit();
                
                Log::info("[DELETE] El pedido con id $shopDelivery se ha eliminado correctamente  -- Status petición: 200");
                return response()->json('OK', 200);
            } else {
                Log::error("[DELETE] Bad request. Parameter: $shopDelivery -- Status petición: 400");
                return response()->json('Bad request', 400);
            }

        } catch(ModelNotFoundException $e) {
            Log::error("[DELETE] No existe el pedido: $shopDelivery -- Status petición: 404");
            return response()->json('Not found', 404);

        }catch( \Exception $e) {
            DB::rollBack();
            
            Log::error("[DELETE] Error al eliminar el pedido  -- Status petición: 500");
            return response()->json('Not found', 500);
        }
    }
}
