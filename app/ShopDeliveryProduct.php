<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopDeliveryProduct extends Model
{
    use SoftDeletes;
    protected $table = 'shop_delivery_product';
    protected $hidden = ['deleted_at'];

    //no especifican que necesitemos timestamps en la bd
    public $timestamps = false;
    
    /**
     * Valores asignables en la base de datos
     *
     * @var array
     */
    protected $fillable = ['delivery_id', 'id_article', 'units', 'price_unit', 'price_total'];

    /**
     * Obtener la entraga a la que pertenece el producto
     */
    public function delivery() {
        return $this->belongsTo('App\ShopDelivery');
    }
}
