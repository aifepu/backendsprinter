
# Prueba PHP Backend Sprinter

  

A continuación se explicará la instalación y uso de la prueba de Backend de la empresa **Sprinter**.

  

## Herramientas utilizadas

- Sistema operativo: MacOs

- FrameWork PHP utilizado: Laravel.

- Entorno para desarrollar: Laravel HomeStead & vagrant

- Control de versiones: GitLab

- Editor de código: Visual Studio Code

- Motor de base de datos: MYSQL

- Caché para peticiones: redis

- Comprobación de URL's para peticiones: PostMan

  

## Pre-requisitos

- Tener las claves públicas copiadas en la siguiente ruta: ~/.ssh/id_rsa.pub

- Tener el proyecto de Homestead clonado en el equipo

- Tener vagrant instalado en el equipo >= v2.2.4

- Tener el repositorio de código "backendsprinter" clonado en la siguiente ruta: ~/Documents/pruebaTecnica/

- Nota: si se quiere clonar el repositorio en algún otro directorio, habrá que cambiar las rutas en el archivo de configuración Homestead.yaml

  

## Instalación del entorno

Ya que uno de los requerimientos era el uso de Laravel, se ha utilizado la instalación del **Homestead** de dicho framework para la realización de la prueba, ya que proporciona todas las herramientas necesarias para comenzar el desarrollo.

  

La documentación de Laravel Homestead se puede encontrar en el siguiente enlace: [HomeStead](https://laravel.com/docs/5.8/installation)

  

Para el desarrollo de la aplicación, se añadirá al fichero de arranque del HomeStead la siguiente configuración:

```

---

ip: "192.168.10.10"

memory: 2048

cpus: 2

provider: virtualbox

  

authorize: ~/.ssh/id_rsa.pub

  

keys:

- ~/.ssh/id_rsa

  

folders:

- map: ~/Documents/pruebaTecnica/

to: /home/vagrant/pruebaTecnica/

  

sites:

- map: backsprinter.es

to: /home/vagrant/pruebaTecnica/backendsprinter/public

  

databases:

- sprinterBackend

  

features:

- mariadb: false

- ohmyzsh: false

- webdriver: false

  

# ports:

# - send: 50000

# to: 5000

# - send: 7777

# to: 777

# protocol: udp

```

Como se puede ver en el archivo de configuración, se creará un nuevo site para desarrollo en local llamado ***backsprinter.es***. El código de la prueba será referenciado dentro de la máquina virtualizada en la ruta: /home/vagrant/pruebaTecnica/ en el proyecto ***backendsprinter***.

  

Además, se ha configurado la creación de la base de datos en el apartado databases. En este caso se llamará: sprinterBackend.

  

Por último, hay que añadir al archivo de rutas /etc/hosts la siguiente dirección:

`192.168.10.10 backsprinter.es`
  

A continuación ya se puede iniciar el entorno de desarrollo desde la carpeta donde se haya instalado el proyecto de Homestead, con el siguiente comando:

    vagrant up --provision

  

## Creación del proyecto

  

> Nota importante: este apartado solamente ha sido necesario para la creación del proyecto. Para el despliegue del entorno, bastará con clonar el proyecto en dentro de la ruta que hemos indicado en el Homestead.yaml (~/Documents/pruebaTecnica/)

  

Una vez tengamos nuestro entorno disponible, se podrá acceder a la máquina virtualizada con el siguiente comando:

    vagrant ssh

Según la configuración realizada en el Homestead.yaml, iremos al siguiente directorio: /home/vagrant/pruebaTecnica/. El proyecto será creado con la versión que se ha especificado, Laravel 5.8

Una vez dentro del mismo, crearemos un proyecto en blanco de Laravel:

    composer create-project laravel/laravel backendsprinter "5.8" --prefer-dist


Con esto ya se tendrá disponible un proyecto de Laravel para comenzar el desarrollo.

  

## Control de versiones

  

Durante todo el desarrollo, se utilizará la rama dev como rama de desarrollo. Cuando el proyecto esté listo, la rama dev se mergeará con master para pasar a ser la versión de release.

  

Con este proceso se pretende emular un pequeño escenario en el que se usa un flujo de git controlado y un proceso de integración continua durante el desarrollo.

  

## Base de datos. Migraciones y modelos.

  

Para la creación de las tablas que compondrán la base de datos se utilizará artisan de Laravel.

  

En primer lugar habrá que crear los archivos de migraciones para las tablas "shop_delivery" y "shop_delivery_product". Para ello, se ejecutará "vagrant ssh" en el directorio de Homestead para acceder a la máquina virtual y habrá que ubicarse en el directorio donde se encuentra el proyecto. Una vez dentro del mismo se ejecutará la siguiente orden:

  

    php artisan make:migration create_shop_delivery_table
    php artisan make:migration create_shop_delivery_product_table

  

Se añadirán a continuación todos los campos necesarios en el archivo de migraciones y se ejecutará:

  

    php artisan migrate

  

Con esto tendremos creadas las tablas necesarias para realizar la prueba técnica.

  

Para realizar el método delete, se ha optado por utilizar softDelete de Laravel, ya que en la especificación del proyecto no se dice que haya que eliminar los registros de base de datos cuando un pedido es eliminado. Para ello, se han creado dos nuevas migraciones, en las cuales se añade la nueva columna necesaria en ambas tablas de la base de datos. De este modo, se podría hacer rollback de una forma sencilla de la base de datos si hubiera algún tipo de error.

    php artisan make:migration add_softdelete_column_table_shop_delivery --table=shop_delivery
    php artisan make:migration add_softdelete_column_table_shop_delivery_product --table=shop_delivery_product

  

Para la creación de los modelos que conectan con estas tablas se lanzará lo siguiente:

  

    php artisan make:model Shop_delivery
    php artisan make:model Shop_delivery_product

  

## Controlador de la aplicación

  

Por último crearemos en controlador para nuestra aplicación:

  

    php artisan make:controller ShopDeliveryController --resource

  
## Uso del proyecto

Postman: https://www.getpostman.com/

Las peticiones han sido comprobadas mediante el uso de Postman. Se ha exportado un archivo .json con ejemplos de uso para peticiones de tipo GET, POST y DELETE ubicado en la ruta app\tests\BackendSprinter.postman_collection.json

Una vez levantado el proyecto, podremos hacer peticiones al dominio ***backsprinter.es***. Para correr las peticiones en Postman, podemos importar directamente la colección y realizar las peticiones de ejemplo para comprobar que el proyecto funciona.
La ventaja de realizar este tipo de pruebas, es que si trabajamos en equipo con solo compartir el archivo con los ejemplos es muy sencillo ver ejemplos de uso del proyecto. Además, la herramienta permite la creación de variables de entorno y algunas configuraciones para probar distintos entornos con la misma colección.

### Testing con Postman

Para ejecutar los test con Postman, se ha de importar la colección que se ha añadido en el directorio tests/Postman/. Una vez dentro , cada vez que ejecutemos las peticiones veremos un apartado de "test results". También se pueden lanzar todos a la vez. Para ello modificamos las peticiones de GET para un indicar un id de pedido existente en la petición y DELETE para indicar de nuevo un pedido existente en la petición.

Cuando tengamos listas nuestras peticiones, pulsamos la opción "run" y se abrirá el "collection runner" de Postman. Se adjuntan 2 capturas, una del "Collection Runner" y otra con los resultados de ejecución. Dichas capturas están en la carpeta tests/Postman/.

Los tests que se han ejecutado son muy sencillos, simplemente para comprobar el cuerpo de la respuesta y los códigos HTTP que se devuelven. Además también se ha introducido una prueba de velocidad en cada petición para comprobar que ninguna tarda más de 2 segundos en ejecutarse.


## Defensa del proyecto

  

A continuación se explicarán algunas de las decisiones principales que se han tomado a lo largo del desarrollo del proyecto.

  

- Creación y borrado de pedidos

  

Se ha realizado el desarrollo del proyecto que un pedido no puede ser creado sin tener, como mínimo, una línea de pedido asociada. Es por ello que en los modelos creados a partir de las migraciones se ha establecido la relación entre ambos objetos.

  

Es por ello que en el método "store" del controlador, el cual nos creará un nuevo pedido en base de datos con sus líneas de pedido asociadas, se ha optado por utilizar transacciones para la comunicación con base de datos. De esta forma nos aseguramos de que no tendremos inconsistencias en nuestra base de datos, por ejemplo, con líneas de pedido que no pertenecen a ningún pedido.

  

Para el borrado de pedidos en nuestra base de datos, se ha optado por utilizar softdelete, el cual no elimina el registro de la base de datos, ya que no estaba explícitamente especificado. Para saber si un pedido se encuentra en estado eliminado, se debería consultar el campo "deleted_at" y comprobar que este no es nulo.

  

En un futuro, para limpiar la base de datos podría lanzarse una tarea CRON o modificar el desarrollo para que sí elimine las tuplas de BD si fuera necesario.

  

Además, se ha sobreescrito el método boot() en el modelo ShopDelivery para que cuando un pedido sea eliminado, se eliminen también sus líneas de pedido asociadas.

  

En el método "delete" del controlador, se tiene en cuenta si el registro a eliminar existe en base de datos, para tener un control más exhaustivo de los códigos HTTP devueltos por nuestra aplicación.

  
  

- Archivo routes/api

  

En la especificación del proyecto, se dice que hay que crear un api de restful que realice las siguientes acciones: crear un pedido, eliminar un pedido y consultar un pedido. Como no se especifica que se puede realizar una modificación de pedido, se ha decidido prescindir de la ruta para realizar update. Además, tampoco se ha realizado el desarrollo necesario.

  

Para facilitar la consulta de los pedidos que no han sido eliminados, en la ruta principal podemos consultar todos los que tenemos en base de datos.

  

De tal modo, que si se lanza el siguiente comando en el directorio del proyecto:

  

php artisan route:list

  

Se pueden consultar las siguientes rutas de aplicación:

    +--------+----------+-------------------------+------------------+-----------------------------------------------------+------------+
    
    | Domain | Method | URI | Name | Action | Middleware |
    
    +--------+----------+-------------------------+------------------+-----------------------------------------------------+------------+
    
    | | GET|HEAD | api/delivery | delivery.index | App\Http\Controllers\ShopDeliveryController@index | api |
    
    | | POST | api/delivery | delivery.store | App\Http\Controllers\ShopDeliveryController@store | api |
    
    | | GET|HEAD | api/delivery/{delivery} | delivery.show | App\Http\Controllers\ShopDeliveryController@show | api |
    
    | | DELETE | api/delivery/{delivery} | delivery.destroy | App\Http\Controllers\ShopDeliveryController@destroy | api |
    
    +--------+----------+-------------------------+------------------+-----------------------------------------------------+------------+

  

- Archivo .env y caché en peticiones

  

Para la creación de la configuración en el entorno de desarrollo se han utilizado los siguientes valores:

  

```
APP_NAME=SprinterBackend
APP_ENV=local
APP_KEY=base64:csUjAz2OhlmdIdHLR13KW3dADk7qCIY1pDZ55Obvk4c=
APP_DEBUG=true
APP_URL=http://backsprinter.es
LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=sprinterBackend
DB_USERNAME=root
DB_PASSWORD=secret

BROADCAST_DRIVER=log
CACHE_DRIVER=redis
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

```

  

Para la caché en las peticiones de nuestra api, se ha optado por utilizar redis. Además, se ha utilizado una forma bastante sencilla para almacenar en caché, controlando las entradas de caché desde el controlador. Se podría realizar este control también en base a las rutas.

  

- Logs

Para la escritura de logs, se ha utilizado el sistema de logging que trae laravel por defecto. Los archivos de log son creados dentro del directorio de nuestro proyecto en la carpeta "storage/logs". Aquí son creados según las peticiones por fecha.

  

Los mensajes que se pueden encontrar en estos archivos de log seguirán esta nomenclatura, pudiendo ser de tipo "info" o de tipo "error":

  

>[GET|SHOW|POST|DELETE] mensaje asociado -- Status petición: estado HTTP de la petición

  

Para los mensajes que indican escritura en caché se sigue esta nomenclatura:

  

>[CACHE] mensaje asociado
