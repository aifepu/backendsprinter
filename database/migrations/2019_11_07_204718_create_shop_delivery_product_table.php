<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopDeliveryProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_delivery_product', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('delivery_id');
            $table->string('id_article', 255);
            $table->integer('units');
            $table->float('price_unit', 10, 2);
            $table->float('price_total', 10, 2);
            Schema::enableForeignKeyConstraints();
        });

        Schema::table('shop_delivery_product', function($table) {
            $table->foreign('delivery_id')->references('id')->on('shop_delivery');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_delivery_product');
    }
}
